<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr" sourcelanguage="en">
<context>
    <name>Dialog</name>
    <message>
        <location filename="../gui/dlg_add_duckdb_layer.ui" line="26"/>
        <source>DuckDB</source>
        <translation>DuckDB</translation>
    </message>
    <message>
        <location filename="../gui/dlg_add_duckdb_layer.ui" line="35"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Database&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Base de données&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../gui/dlg_add_duckdb_layer.ui" line="42"/>
        <source>Add Layer</source>
        <translation>Ajouter une couche</translation>
    </message>
    <message>
        <location filename="../gui/dlg_add_duckdb_layer.ui" line="52"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Projection&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Projection&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../gui/dlg_add_duckdb_layer.ui" line="59"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Table&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Table&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
</context>
<context>
    <name>wdg_qduckdb_settings</name>
    <message>
        <location filename="../gui/dlg_settings.ui" line="14"/>
        <source>QDuckDB - Settings</source>
        <translation>QDuckDB - Paramètres</translation>
    </message>
    <message>
        <location filename="../gui/dlg_settings.ui" line="44"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;PluginTitle - Version X.X.X&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;PluginTitle - Version X.X.X&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../gui/dlg_settings.ui" line="75"/>
        <source>Miscellaneous</source>
        <translation>Divers</translation>
    </message>
    <message>
        <location filename="../gui/dlg_settings.ui" line="124"/>
        <source>Report an issue</source>
        <translation>Créer un ticket</translation>
    </message>
    <message>
        <location filename="../gui/dlg_settings.ui" line="146"/>
        <source>Version used to save settings:</source>
        <translation>Version utilisée pour sauvegarder les paramètres:</translation>
    </message>
    <message>
        <location filename="../gui/dlg_settings.ui" line="168"/>
        <source>Help</source>
        <translation>Aide</translation>
    </message>
    <message>
        <location filename="../gui/dlg_settings.ui" line="190"/>
        <source>Reset setttings to factory defaults</source>
        <translation>Réinitialiser les paramètres par défaut</translation>
    </message>
    <message>
        <location filename="../gui/dlg_settings.ui" line="209"/>
        <source>Enable debug mode.</source>
        <translation>Activer le mode debug.</translation>
    </message>
    <message>
        <location filename="../gui/dlg_settings.ui" line="218"/>
        <source>Debug mode (degraded performances)</source>
        <translation>Mode debug (performances dégradées)</translation>
    </message>
</context>
</TS>
